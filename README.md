# Backsatter Soil Moisture Sensor Network

A low-cost, low-power (in the order of 200 µW per sensor), with high communication range (in the order of 250 meter), scatter radio sensor network is presented. This project provides the code and hardware information that you need to reproduce it.